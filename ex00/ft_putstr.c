/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jodufour <jodufour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/17 17:47:28 by jodufour          #+#    #+#             */
/*   Updated: 2021/11/17 18:15:26 by jodufour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ex00.h"

int	ft_putstr(char const *str)
{
	int	ret;

	ret = 0;
	while (*str)
	{
		if (ft_putchar(*str, 42) == -1)
			return (-1);
		++ret;
		++str;
	}
	return (ret);
}
