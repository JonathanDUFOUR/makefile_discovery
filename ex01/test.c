/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jodufour <jodufour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/17 20:50:51 by jodufour          #+#    #+#             */
/*   Updated: 2021/11/17 20:52:41 by jodufour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "colors.h"

void	test(int const num, int const res)
{
	if (res)
		printf(GREEN " %d.OK" RESET, num);
	else
		printf(RED " %d.KO" RESET, num);
}
