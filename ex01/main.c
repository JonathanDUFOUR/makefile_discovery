/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jodufour <jodufour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/17 20:47:11 by jodufour          #+#    #+#             */
/*   Updated: 2021/11/17 20:50:40 by jodufour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <ctype.h>
#include "ex01.h"

static void	test_ft_isalnum(void)
{
	int	i;

	printf("%20s:", __func__ + 5);
	i = 0;
	while (i < 256)
	{
		test(i, !!isalnum(i) == !!ft_isalnum(i));
		++i;
	}
	printf("\n");
}

static void	test_ft_isspace(void)
{
	int	i;

	printf("%20s:", __func__ + 5);
	i = 0;
	while (i < 256)
	{
		test(i, !!isspace(i) == !!ft_isspace(i));
		++i;
	}
	printf("\n");
}

int	main(void)
{
	test_ft_isalnum();
	test_ft_isspace();
	return (0);
}
