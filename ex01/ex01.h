/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ex01.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jodufour <jodufour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/17 18:53:57 by jodufour          #+#    #+#             */
/*   Updated: 2021/11/17 19:08:51 by jodufour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef EX01_H
# define EX01_H

# include <stdbool.h>

void	test(int const num, int const res);

bool	ft_isalnum(int const c);
bool	ft_isalpha(int const c);
bool	ft_isdigit(int const c);
bool	ft_islower(int const c);
bool	ft_isupper(int const c);
bool	ft_isspace(int const c);

#endif
